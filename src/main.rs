extern crate rand;

use rand::Rng;

const ADJACENT_VAPOR_CAUSING_RAIN: usize = 5;
const VAPOR_PER_BLOCK_PER_STEP: f64 = 1.0;
const VAPER_PER_CLOUD: f64 = 5.0;
const WIDTH: usize = 100;
const STEPS: usize = 30;

fn count_clouds(clouds: &Vec<bool>) -> f64 {
  let mut num_clouds = 0.0;
  for x in clouds {
    if *x {
      num_clouds += 1.0;
    }
  }
  return num_clouds;
}

fn evaporation((clouds, vapor): (Vec<bool>, f64)) -> (Vec<bool>, f64) {
  let num_clouds = count_clouds(&clouds);
  let new_vapor = VAPOR_PER_BLOCK_PER_STEP * (clouds.len() as f64 - num_clouds);
  return (clouds, vapor + new_vapor);
}

fn condensation((clouds, vapor): (Vec<bool>, f64)) -> (Vec<bool>, f64) {
  let mut new_clouds = clouds.clone();

  let num_new_clouds = (vapor / VAPER_PER_CLOUD).floor();
  let vapor = vapor - VAPER_PER_CLOUD * num_new_clouds;

  let num_clouds = count_clouds(&clouds);
  let mut empty_sky = clouds.len() - num_clouds as usize;

  for _ in 0..(num_new_clouds as usize) {
    // pick a random location
    let pos: usize = rand::thread_rng().gen_range(0, empty_sky);

    // insert it
    let mut empty_sky_index = 0;
    let mut index_for_cloud = 0; // hmm but I don't want to initialize this..
    for (sky_index, x) in new_clouds.iter().enumerate() {
      if !*x {
        if empty_sky_index == pos {
          index_for_cloud = sky_index;
          break;
        }
        empty_sky_index += 1
      }
    }

    new_clouds[index_for_cloud] = true;
    empty_sky -= 1;
  }

  return (new_clouds, vapor);
}

fn precipitation((clouds, vapor): (Vec<bool>, f64)) -> (Vec<bool>, f64) {
  let mut new_clouds = clouds.clone();

  let len = clouds.len();
  let mut start = len;
  let mut rain_ranges = vec![];

  for (index, cloud) in clouds.iter().enumerate() {
    if *cloud && start == len {
      start = index;
    } else if !*cloud {
      if start != len && index - start >= ADJACENT_VAPOR_CAUSING_RAIN {
        rain_ranges.push((start, index));
      }
      start = len;
    }
  }

  if start != len && len - start >= ADJACENT_VAPOR_CAUSING_RAIN {
    rain_ranges.push((start, len));
  }

  for (start, end) in rain_ranges {
    for index in start..end {
      new_clouds[index] = false;
    }
  }

  return (new_clouds, vapor);
}

fn step(state: (Vec<bool>, f64)) -> (Vec<bool>, f64) {
  let state = evaporation(state);
  let state = condensation(state);
  print_clouds(&state.0);
  let state = precipitation(state);
  print_clouds(&state.0);

  return state;
}

fn print_clouds(clouds: &Vec<bool>) {
  print!("[ ");
  let mut count = 0;
  for cloud in clouds {
    print!("{}", if *cloud { "*" } else { " " });
    if *cloud {
      count += 1;
    }
  }
  println!(" ] {}", count);
}

fn main() {
  let clouds = std::iter::repeat(false).take(WIDTH).collect();
  let vapor = 0.0;
  let mut state = (clouds, vapor);

  for _ in 0..STEPS {
    state = step(state);
  }
}
